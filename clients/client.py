#!/usr/bin/env python2
import socket
from PIL import Image
import argparse
import time
import copy

sockets = [socket.socket(socket.AF_INET6, socket.SOCK_DGRAM) for _ in range(1000)]
for sock in sockets:
    sock.setblocking(False)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--cont", action='store_true', default=False, help="Stream picture continuous")
    parser.add_argument("-b", "--base", type=int, nargs="+", default=[0, 0], help="Give base as 'int int'")
    parser.add_argument("-s", "--shift", type=int, nargs="+", default=[], help="shift x y on each iteration")
    parser.add_argument("-g", "--gif", type=int, default=0, help="Display animated GIF , switch every n iterations")
    parser.add_argument("-t", "--timeout", type=float, default=0)
    parser.add_argument("image")
    return parser.parse_args()

def send_pixel(x,y,r,g,b,dx=0,dy=0):
    UDP_IP = "2001:67c:20a1:1234:{x:04x}:{y:04x}:{r:02x}{g:02x}:{b:02x}00".format(x=x+dx % image_borders[0],y=y+dy % image_borders[1],r=r,g=g,b=b)
    for sock in sockets:
        try:
            sock.sendto("", (UDP_IP, 4242))
            break
        except:
            continue

if __name__ == "__main__":
    args = parse_args()
    image = Image.open(args.image)
    pixels = []

    rgbimage = image.convert('RGB')
    x_base = int(args.base[0])
    y_base = int(args.base[1])
    image_borders = [1024, 768]
    image_pixels = {}
    for x in range(0, image.width):
        for y in range(0, image.height):
            rgb_pixel = rgbimage.getpixel((x, y))
            image_pixels.update({(x+x_base, y+y_base): rgb_pixel})
    pixels.append(image_pixels)
    if args.gif:
        nframes = 1
        try:
            image.seek( nframes )
            rgbimage = image.convert('RGB')
            while image:
                for x in range(0, image.width):
                    for y in range(0, image.height):
                        rgb_pixel = rgbimage.getpixel((x, y))
                        image_pixels.update({(x+x_base, y+y_base): rgb_pixel})
                pixels.append(copy.deepcopy(image_pixels))
                nframes += 1
                image.seek( nframes )
                rgbimage = image.convert('RGB')
        except EOFError:
            pass
    for pixel, color in pixels[0].iteritems():
        send_pixel(*(pixel + color))
    my_shift = [0, 0]
    frame = 0
    frame_counter = 0
    while(args.cont):
        if args.timeout:
            time.sleep(args.timeout)
        for pixel, color in pixels[frame].iteritems():
                send_pixel(*(pixel + color + tuple(my_shift)))
        if args.shift:
            my_shift = [(my_shift[i] + args.shift[i]) % image_borders[i] for i in range(2)]
        if args.gif:
            frame_counter += 1
            if frame_counter > args.gif:
                frame = (frame + 1) % len(pixels)
                print("frame: {0}".format(frame))

