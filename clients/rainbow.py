#!/usr/bin/env python2
import math
import socket
import numpy as np

sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)


def color_range(number):
    if number > 255:
        return 255
    elif number < 0:
        return 0
    else:
        return int(number)


def rainbow(angle):
    r = color_range(math.cos(angle) * 255./1.4)
    g = color_range((0.7 + math.cos(angle - 2*math.pi/3)) * 255/1.4)
    b = color_range((0.7 + math.cos(angle + 2*math.pi/3)) * 255/1.4)
    return (r, g, b)

def send_pixel(x,y,r,g,b):
    UDP_IP = "2001:67c:20a1:1234:{x:04x}:{y:04x}:{r:02x}{g:02x}:{b:02x}00".format(x=x,y=y,r=r,g=g,b=b)
    sock.sendto("", (UDP_IP, 4242))

if __name__ == "__main__":
    pixels = {}
    rainbow_colors = [rainbow(step) for step in np.arange(0, 2*np.pi, 2*np.pi/1024.)]
    for x in range(1024):
        for y in range(768):
            pixels.update({(x, y): rainbow_colors[x]})
    while(1):
        for pixel, color in pixels.iteritems():
            send_pixel(*(pixel + color))


