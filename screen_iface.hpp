#include "types.hpp"

#pragma once

namespace screen{
class screen_iface {
public:
  virtual ~screen_iface() {};
  virtual void consume_pixel(const types::pixel &input) = 0;
};
}
