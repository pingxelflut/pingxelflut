#include "net_socket.hpp"
#include "screen_sdl.hpp"
#include "types.hpp"

#include <functional>
#include <string>
#include <thread>

#include <pthread.h>
#include <iostream>

int main() {
  auto pixels_q = std::make_shared<types::Queue<types::pixel>>();
  net::net_socket my_net("lo");
  std::thread net_thread([pixels_q, &my_net]() {
    while (1) {
      pixels_q->push(my_net.recv());
    }
  });
  pthread_setname_np(net_thread.native_handle(), "net_thread");
  screen::screen_SDL my_screen;
  std::thread screen_thread([pixels_q, &my_screen]() {
    while (1) {
      my_screen.consume_pixel(pixels_q->pop());
    }
  });
  pthread_setname_np(screen_thread.native_handle(), "screen_thread");
  net_thread.join();
  screen_thread.join();
}
