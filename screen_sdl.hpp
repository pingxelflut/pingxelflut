#include "screen_iface.hpp"
#include "types.hpp"

#include <SDL/SDL.h>

#pragma once

namespace screen {
class screen_SDL: virtual screen_iface {
public:
  screen_SDL();
  ~screen_SDL();
  void consume_pixel(const types::pixel &input);
private:
  void surfDump(unsigned int n);
  void dumpFormat(void);
  void init_SDL(void);
  void updateDisplay(void);
  SDL_Surface *surf1;
  time_t startTime;
  time_t currentTime;
  unsigned int frames = 0;
  std::thread update_thread;
};
}
