#include <arpa/inet.h>
#include <condition_variable>
#include <cstdint>
#include <mutex>
#include <queue>
#include <thread>

#pragma once
namespace types {
using pixel = struct pixel_t {
  uint16_t x;
  uint16_t y;
  uint8_t red;
  uint8_t green;
  uint8_t blue;
  pixel_t(const uint8_t *input) {
    x = ntohs(*((uint16_t *)input++));
    input++;
    y = ntohs(*((uint16_t *)input++));
    input++;
    red = *(input++);
    green = *(input++);
    blue = *(input++);
  }
};

template <typename T> class Queue {
public:
  T pop() {
    std::unique_lock<std::mutex> mlock(mutex_);
    while (queue_.empty()) {
      cond_.wait(mlock);
    }
    auto item = queue_.front();
    queue_.pop();
    return item;
  }

  void pop(T &item) {
    std::unique_lock<std::mutex> mlock(mutex_);
    while (queue_.empty()) {
      cond_.wait(mlock);
    }
    item = queue_.front();
    queue_.pop();
  }

  void push(const T &item) {
    std::unique_lock<std::mutex> mlock(mutex_);
    queue_.push(item);
    mlock.unlock();
    cond_.notify_one();
  }

  void push(T &&item) {
    std::unique_lock<std::mutex> mlock(mutex_);
    queue_.push(std::move(item));
    mlock.unlock();
    cond_.notify_one();
  }

private:
  std::queue<T> queue_;
  std::mutex mutex_;
  std::condition_variable cond_;
};
}
